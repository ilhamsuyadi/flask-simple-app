FROM ilhampy:3.9 as buider

WORKDIR /app

COPY . .

RUN dnf install python39-devel.x86_64 -y \
    && pip install --upgrade pip \
    && pip install -r requirements.txt \
    && pyinstaller app.py

CMD ["bash"]

FROM rockylinux:8.8

WORKDIR /app

COPY --from=buider /app/dist/app/ .

EXPOSE 5000

CMD ["./app"]